import RPi.GPIO as GPIO
import socket
import time
def internet(host="8.8.8.8", port=53, timeout=3):
    """
    Host: 8.8.8.8 (google-public-dns-a.google.com)
    OpenPort: 53/tcp
    Service: domain (DNS/TCP)
    """
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return True
    except socket.error as ex:
        print(ex)
        return False

GREEN = 20
RED = 21
ALL = (GREEN, RED)

def set_green():
    GPIO.output(GREEN, 1)
    GPIO.output(RED, 0)


def set_red():
    GPIO.output(GREEN, 0)
    GPIO.output(RED, 1)


def set_off():
    GPIO.output(GREEN, 0)
    GPIO.output(RED, 0)


if __name__ == '__main__':
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ALL, GPIO.OUT)
    while(True):
        if internet():
            set_green()
        else:
            set_red()
        time.sleep(0.1)
        set_off()
        time.sleep(8)

